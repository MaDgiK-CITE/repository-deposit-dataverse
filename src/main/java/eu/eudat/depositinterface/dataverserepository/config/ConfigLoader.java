package eu.eudat.depositinterface.dataverserepository.config;

import java.util.List;

public interface ConfigLoader {
    byte[] getLogo(String repositoryId);
    List<DataverseConfig> getDataverseConfig();
}
