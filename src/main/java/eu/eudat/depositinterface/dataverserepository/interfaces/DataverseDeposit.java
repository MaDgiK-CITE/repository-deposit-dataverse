package eu.eudat.depositinterface.dataverserepository.interfaces;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.researchspace.dataverse.api.v1.DataverseAPI;
import com.researchspace.dataverse.api.v1.DataverseConfig;
import com.researchspace.dataverse.entities.*;
import com.researchspace.dataverse.entities.facade.DatasetAuthor;
import com.researchspace.dataverse.entities.facade.DatasetContact;
import com.researchspace.dataverse.entities.facade.DatasetDescription;
import com.researchspace.dataverse.entities.facade.DatasetFacade;
import com.researchspace.dataverse.http.DataverseAPIImpl;
import eu.eudat.depositinterface.dataverserepository.config.ConfigLoader;
import eu.eudat.depositinterface.models.DMPDepositModel;
import eu.eudat.depositinterface.models.FileEnvelope;
import eu.eudat.depositinterface.repository.RepositoryDeposit;
import eu.eudat.depositinterface.repository.RepositoryDepositConfiguration;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.*;
import java.util.stream.Collectors;

import static com.researchspace.dataverse.entities.Version.MAJOR;
import static com.researchspace.dataverse.entities.Version.MINOR;

@Component
public class DataverseDeposit implements RepositoryDeposit {
    private static final Logger logger = LoggerFactory.getLogger(DataverseDeposit.class);
    private static final ObjectMapper objectMapper = new ObjectMapper();

    private final ConfigLoader configLoader;
    private final Environment environment;

    @Autowired
    public DataverseDeposit(ConfigLoader configLoader, Environment environment){
        this.configLoader = configLoader;
        this.environment = environment;
    }

    @Override
    public String deposit(String repositoryId, DMPDepositModel dmpDepositModel, String repositoryAccessToken) throws Exception {

        eu.eudat.depositinterface.dataverserepository.config.DataverseConfig jsonConfig = this.configLoader.getDataverseConfig().stream().filter(x -> x.getRepositoryId().equals(repositoryId)).findFirst().orElse(null);


        if(jsonConfig != null) {

            DataverseAPI api = new DataverseAPIImpl();
            DataverseConfig config = new DataverseConfig(new URL(jsonConfig.getServer()), jsonConfig.getApiToken(), jsonConfig.getParentDataverseAlias());
            api.configure(config);


            String dmpDescription = dmpDepositModel.getDescription();
            if (dmpDescription == null || dmpDescription.isEmpty()) {
                dmpDescription = "-";
            }

            String doi;
            DatasetFacade dataset = DatasetFacade.builder()
                    .title(dmpDepositModel.getLabel())
                    .authors(dmpDepositModel.getUsers().stream().map(x -> DatasetAuthor.builder().authorName(x.getUser().getName()).build()).collect(Collectors.toList()))
                    .contacts(dmpDepositModel.getUsers().stream().map(x -> DatasetContact.builder().datasetContactEmail(x.getUser().getEmail()).build()).collect(Collectors.toList()))
                    .subject("Other")
                    .description(DatasetDescription.builder().description(dmpDescription).build())
                    .languages(new ArrayList<>())
                    .depositor("")
                    .build();

            if (dmpDepositModel.getPreviousDOI() == null || dmpDepositModel.getPreviousDOI().isEmpty()) {
                Identifier id = api.getDataverseOperations().createDataset(dataset, jsonConfig.getParentDataverseAlias());

                doi = api.getDatasetOperations().getDataset(id).getDoiId().orElse(null);

                this.uploadFiles(jsonConfig, dmpDepositModel, doi);

                api.getDatasetOperations().publishDataset(id, MAJOR);
            } else {
                Map<String, Object> datasetJson = this.getDatasetIdentifier(jsonConfig, dmpDepositModel.getPreviousDOI());
                Identifier id = new Identifier();
                id.setId(((Integer) datasetJson.get("id")).longValue());
                JsonNode jsonNode = objectMapper.convertValue(datasetJson, JsonNode.class);
                JsonNode latestVersion = jsonNode.get("latestVersion");
                JsonNode files = latestVersion.get("files");
                if (files.isArray()) {
                    for (JsonNode file : files) {
                        int fileId = file.get("dataFile").get("id").asInt();
                        this.deleteFile(jsonConfig, fileId);
                    }
                }

                this.uploadFiles(jsonConfig, dmpDepositModel, dmpDepositModel.getPreviousDOI());

                api.getDatasetOperations().updateDataset(dataset, id);
                DataverseResponse<PublishedDataset> publishedDataset = api.getDatasetOperations().publishDataset(id, MAJOR);
                doi = publishedDataset.getData().getAuthority() + "/" + publishedDataset.getData().getIdentifier();
            }


            return doi;

        }

        return null;

    }

    private void deleteFile(eu.eudat.depositinterface.dataverserepository.config.DataverseConfig jsonConfig, int fileId){
        HttpHeaders headers = this.createBasicAuthHeaders(jsonConfig.getApiToken(), "");
        String serverUrl = jsonConfig.getServer() + "/dvn/api/data-deposit/v1.1/swordv2/edit-media/file/" + fileId;
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.exchange(serverUrl, HttpMethod.DELETE, new HttpEntity<>(headers), Object.class);
    }

    private HttpHeaders createBasicAuthHeaders(String username, String password) {
        return new HttpHeaders() {{
            String auth = username + ":" + password;
            byte[] encodedAuth = Base64.getEncoder().encode(
                    auth.getBytes(StandardCharsets.UTF_8));
            String authHeader = "Basic " + new String(encodedAuth);
            set("Authorization", authHeader);
        }};
    }

    private void uploadFiles(eu.eudat.depositinterface.dataverserepository.config.DataverseConfig jsonConfig, DMPDepositModel dmpDepositModel, String doi) throws IOException {
        this.uploadFile(jsonConfig, dmpDepositModel.getPdfFile().getFilename(), dmpDepositModel.getPdfFile().getFile(), doi);

        FileEnvelope rdaJsonEnvelope = dmpDepositModel.getRdaJsonFile();
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.setContentLength(rdaJsonEnvelope.getFile().length());
        responseHeaders.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        responseHeaders.set("Content-Disposition", "attachment;filename=" + rdaJsonEnvelope.getFilename());
        responseHeaders.set("Access-Control-Expose-Headers", "Content-Disposition");
        responseHeaders.get("Access-Control-Expose-Headers").add("Content-Type");

        byte[] content = Files.readAllBytes(rdaJsonEnvelope.getFile().toPath());

        ResponseEntity<byte[]> jsonFile = new ResponseEntity<>(content, responseHeaders, HttpStatus.OK);

        String contentDisposition = jsonFile.getHeaders().get("Content-Disposition").get(0);
        String jsonFileName = contentDisposition.substring(contentDisposition.lastIndexOf('=')  + 1);
        File rdaJson = new File(this.environment.getProperty("dataverse_plugin.storage.temp") + jsonFileName);
        OutputStream output = new FileOutputStream(rdaJson);
        try {
            output.write(Objects.requireNonNull(jsonFile.getBody()));
            output.flush();
            output.close();
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
        }
        this.uploadFile(jsonConfig, jsonFileName, rdaJson, doi);
        Files.deleteIfExists(rdaJson.toPath());

        if(dmpDepositModel.getSupportingFilesZip() != null) {
            this.uploadFile(jsonConfig, dmpDepositModel.getSupportingFilesZip().getName(), dmpDepositModel.getSupportingFilesZip(), doi);
        }
    }

    private Map<String, Object> getDatasetIdentifier(eu.eudat.depositinterface.dataverserepository.config.DataverseConfig jsonConfig, String previousDOI) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("X-Dataverse-key", jsonConfig.getApiToken());
        String serverUrl = jsonConfig.getServer() + "/api/datasets/:persistentId?persistentId=doi:" + previousDOI;
        RestTemplate restTemplate = new RestTemplate();
        return (Map<String, Object>) restTemplate.exchange(serverUrl, HttpMethod.GET, new HttpEntity<>(headers), Map.class).getBody().get("data");
    }

    private void uploadFile(eu.eudat.depositinterface.dataverserepository.config.DataverseConfig jsonConfig, String filename, File file, String doi) throws IOException {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        headers.set("X-Dataverse-key", jsonConfig.getApiToken());
        MultiValueMap<String, String> fileMap = new LinkedMultiValueMap<>();
        ContentDisposition contentDisposition = ContentDisposition
                .builder("form-data")
                .name("file")
                .filename(filename)
                .build();
        fileMap.add(HttpHeaders.CONTENT_DISPOSITION, contentDisposition.toString());
        HttpEntity<byte[]> fileEntity = new HttpEntity<>(Files.readAllBytes(file.toPath()), fileMap);
        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        body.add("file", fileEntity);
        body.add("jsonData", "{\"restrict\":\"false\", \"tabIngest\":\"false\"}");
        HttpEntity<MultiValueMap<String, Object>> requestEntity
                = new HttpEntity<>(body, headers);

        String serverUrl = jsonConfig.getServer() + "/api/datasets/:persistentId/add?persistentId=doi:" + doi;

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<Object> resp = restTemplate.postForEntity(serverUrl, requestEntity, Object.class);
    }

    @Override
    public List<RepositoryDepositConfiguration> getConfiguration() {
        List<eu.eudat.depositinterface.dataverserepository.config.DataverseConfig> dataverseConfigs = this.configLoader.getDataverseConfig();
        return dataverseConfigs.stream().map(eu.eudat.depositinterface.dataverserepository.config.DataverseConfig::toRepoConfig).collect(Collectors.toList());
    }

    @Override
    public String authenticate(String repositoryId, String code) {
        return null;
    }

    @Override
    public String getLogo(String repositoryId) {
        RepositoryDepositConfiguration conf = this.getConfiguration().stream().filter(x -> x.getRepositoryId().equals(repositoryId)).findFirst().orElse(null);
        if(conf != null) {
            if (conf.isHasLogo()) {
                byte[] logo = this.configLoader.getLogo(repositoryId);
                return (logo != null && logo.length != 0) ? Base64.getEncoder().encodeToString(logo) : null;
            }
        }
        return null;
    }
}
