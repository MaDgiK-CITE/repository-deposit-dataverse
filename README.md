# Using dataverse repository with Argos

The repository-deposit-dataverse module implements the [https://code-repo.d4science.org/MaDgiK-CITE/repository-deposit-base](https://) interface for the dataverse repository.

## Setup

After creating the jar from the project, environment variables should be set since they are used in the application.properties
1) STORAGE_TMP_DATAVERSE - a temporary storage needed
2) CONFIGURATION_DATAVERSE - path to json file which includes the configuration for the repository

### JSON configuration file

The following fields should be set:<br>
**depositType** - an integer representing how the dmp user can deposit in the repository,<br>
a. **0** stands for system deposition meaning the dmp is deposited using argos credentials to the repository,<br>
b. **1** stands for user deposition in which the argos user specifies his/her own credentials to the repository,<br>
c. **2** stands for both ways deposition if the repository allows the deposits of dmps to be made from both argos and users accounts<br>
note: depositType should be set to **0** since dataverse does not provide oauth2 protocol but, instead, uses api tokens<br>
**repositoryId** - unique identifier for the repository<br>
**apiToken** - api token provided for the depositions<br>
**repositoryUrl** - repository's api url e.g. "https://demo.dataverse.org/api/"<br>
**repositoryRecordUrl** - repository's record url, this url is used to index dmps that are created e.g. "https://demo.dataverse.org/dataset.xhtml?persistentId=doi:"<br>
**server** - repository's server url e.g. "https://demo.dataverse.org"<br>
**parentDataverseAlias** - dataverse alias in which all dmps that are deposited will be resided, **note**: the dataverse alias used should be published before making any dmp deposit<br>
**hasLogo** - if the repository has a logo<br>
